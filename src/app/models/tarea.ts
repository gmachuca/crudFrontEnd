import { Usuario } from "./usuario";

export class Tarea {
  id: number;
  titulo: string;
  descripcion:string;
  createAt:string;
  estado: string;
  usuario: Usuario;

}
