import { Component, OnInit } from '@angular/core';
import { Tarea } from '../models/tarea';
import { TareaService } from '../service/tarea.service';
import swal from 'sweetalert2'
import { TokenService } from '../service/token.service';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html'
})
export class TareasComponent implements OnInit {

  tareas: Tarea[];
  roles: string[];
  isAdmin = false;
  constructor(private tareaService: TareaService,
    private tokenService: TokenService) { }

  ngOnInit() {
    this.tareaService.getTareas().subscribe(
      tareas => this.tareas = tareas
    );
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol => {
      if (rol === 'ROLE_ADMIN' || rol === 'ROLE_USER') {
        this.isAdmin = true;
      }
    });
  }

  delete(tarea: Tarea): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar al tarea ${tarea.titulo} ${tarea.descripcion}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.tareaService.delete(tarea.id).subscribe(
          response => {
            this.tareas = this.tareas.filter(cli => cli !== tarea)
            swal(
              'Tarea Eliminado!',
              `Tarea ${tarea.titulo} eliminado con éxito.`,
              'success'
            )
          }
        )

      }
    })
  }

}
