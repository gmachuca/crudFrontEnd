import { Component, OnInit } from '@angular/core';
import {Tarea} from '../models/tarea'
import {TareaService} from '../service/tarea.service'
import {Router, ActivatedRoute} from '@angular/router'
import swal from 'sweetalert2'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Usuario } from '../models/usuario'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private tarea: Tarea = new Tarea();
  private titulo:string = "Crear Tarea"
  usuarios: Usuario[];

  constructor(private tareaService: TareaService,
  private router: Router,
private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarTarea();
    this.tareaService.getUsuarios().subscribe(usuarios => this.usuarios = usuarios);

  }

  cargarTarea(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id != undefined){
        this.tareaService.getTarea(id).subscribe( (tarea) => this.tarea = tarea)
      }
    })
  }

  create(): void {
    this.tarea.createAt='2017-11-15';
    this.tareaService.create(this.tarea)
      .subscribe(tarea => {
        this.router.navigate(['/tareas'])
        swal('Nuevo tarea', `Tarea ${tarea.titulo} creado con éxito!`, 'success')
      }
      );
  }

  update():void{
    this.tareaService.update(this.tarea)
    .subscribe( tarea => {
      this.router.navigate(['/tareas'])
      swal('Tarea Actualizado', `Tarea ${tarea.titulo} actualizado con éxito!`, 'success')
    }

    )
  }

  compararUsuario(o1: Usuario, o2: Usuario): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }
}
