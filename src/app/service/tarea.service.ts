import { Injectable } from '@angular/core';
import { Tarea } from '../models/tarea';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Usuario } from '../models/usuario'


@Injectable({
  providedIn: 'root'
})
export class TareaService {
  private urlEndPoint: string = 'http://localhost:8080/tarea/tareas';
  private urlEndPointUsuarios: string = 'http://localhost:8080/tarea';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }
  getUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.urlEndPointUsuarios + '/usuarios');
  }

  getTareas(): Observable<Tarea[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Tarea[])
    );
  }

  create(tarea: Tarea) : Observable<Tarea> {
    return this.http.post<Tarea>(this.urlEndPoint, tarea, {headers: this.httpHeaders})
  }

  getTarea(id): Observable<Tarea>{
    return this.http.get<Tarea>(`${this.urlEndPoint}/${id}`)
  }

  update(tarea: Tarea): Observable<Tarea>{
    return this.http.put<Tarea>(`${this.urlEndPoint}/${tarea.id}`, tarea, {headers: this.httpHeaders})
  }

  delete(id: number): Observable<Tarea>{
    return this.http.delete<Tarea>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders})
  }

}
